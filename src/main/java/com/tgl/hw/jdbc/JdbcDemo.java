package com.tgl.hw.jdbc;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import com.tgl.hw.jdbc.controller.EmployeeController;
import com.tgl.hw.jdbc.model.Employee;
import com.tgl.hw.jdbc.util.EmployeeNotValidException;

public class JdbcDemo {

	private EmployeeController employeeController;

	public static void main(String[] args) throws InterruptedException {

		String filePath = "";
		String dbUrl = "";
		String dbUsername = "";
		String dbPassword = "";

		try (InputStream input = JdbcDemo.class.getClassLoader().getResourceAsStream("config.properties")) {
			Properties prop = new Properties();
			prop.load(input);
			filePath = prop.getProperty("employee.data");
			dbUrl = prop.getProperty("db.connection");
			dbUsername = prop.getProperty("db.username");
			dbPassword = prop.getProperty("db.password");
		} catch (IOException e) {
			e.printStackTrace();
		}

		try (Connection con = DriverManager.getConnection(dbUrl, dbUsername, dbPassword)) {

			JdbcDemo jdbcDemo = new JdbcDemo();
			jdbcDemo.employeeController = new EmployeeController(con, filePath);

			System.out.println("\n=============search case [by ID] fail==========");
			jdbcDemo.search("100");

			System.out.println("\n=============search case [by ID] success==========");
			jdbcDemo.search("3");

			System.out.println("\n=============add case success==========");
			Employee employee = new Employee();
			employee.setHeight(174);
			employee.setWeight(89);
			employee.setEnglishName("Luke-Lan");
			employee.setChineseName("藍浚哲");
			employee.setPhone("1234");
			employee.setEmail("tsiteiceqlr@gmail.com");
			jdbcDemo.insert(employee);

//			make the update time different
			Thread.sleep(3000);

			System.out.println("\n=============update case success==========");
			String employeeId = "57";
			List<Employee> results = jdbcDemo.employeeController.search(Employee.Search.ID, employeeId);
			if (results != null && !results.isEmpty()) {
				employee = results.get(0);
				employee.setHeight(180);
				employee.setChineseName("藍企鵝"); // recover mask name
				boolean isUpdated = false;
				try {
					isUpdated = jdbcDemo.employeeController.update(employee);
				} catch (EmployeeNotValidException e) {
					e.printStackTrace();
				}
				if (isUpdated) {
					System.out.println("employee updated: " + employee);
				}
			}

			System.out.println("\n=============delete [by ID] case fail==========");
			jdbcDemo.delete(100);

			System.out.println("\n=============delete [by Id] case success==========");
			jdbcDemo.delete(57);

			System.out.println("\n=============sort [by phone] case (print only first 10)==========");
			List<Employee> resultList = jdbcDemo.employeeController.sort(Employee.Sort.PHONE);
			int showCnt = 10;
			for (Employee result : resultList) {
				if (showCnt > 0) {
					System.out.printf("%s, %s %n", result.getChineseName(), result.getPhone());
				} else {
					break;
				}
				showCnt--;
			}

			System.out.println("\n=============max [by height] case ==========");
			employee = jdbcDemo.employeeController.max(Employee.Max.HEIGHT);
			System.out.printf("%s, %s %n", employee.getChineseName(), employee.getHeight());

			System.out.println("\n=============min [by height] case ==========");
			employee = jdbcDemo.employeeController.min(Employee.Min.HEIGHT);
			System.out.printf("%s, %s %n", employee.getChineseName(), employee.getHeight());
			
			System.out.println("\n=============TRUNCATE employee success==========");
			jdbcDemo.employeeController.truncate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void search(String employeeId) {
		List<Employee> results = employeeController.search(Employee.Search.ID, employeeId);
		if (results == null || results.isEmpty()) {
			System.out.printf("employee Id[%s] not found%n", employeeId);
		} else {
			for (Employee employee : results) {
				System.out.println("search result by Id: " + employee);
			}
		}
	}

	public void insert(Employee employee) {
		try {
			employee = employeeController.insert(employee);
		} catch (EmployeeNotValidException e) {
			e.printStackTrace();
		}
		System.out.println("employee added: " + employee);
	}

	public void delete(int employeeId) {
		System.out.println("employee size before delete: " + employeeController.size());
		boolean isDeleted = employeeController.delete(employeeId);
		if (isDeleted) {
			System.out.printf("employee deleted by Id: %s%n", employeeId);
		} else {
			System.out.printf("employee Id[%s] not found%n", employeeId);
		}
		System.out.println("employee size after delete: " + employeeController.size());
	}

}
