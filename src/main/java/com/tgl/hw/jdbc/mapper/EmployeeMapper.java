package com.tgl.hw.jdbc.mapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.tgl.hw.jdbc.model.EmployColumon;
import com.tgl.hw.jdbc.model.Employee;

public final class EmployeeMapper {
	
	public Employee convertToModel(ResultSet rs) throws SQLException {
		Employee employee = new Employee();		
		employee.setId(rs.getInt(EmployColumon.ID));
		employee.setHeight(rs.getInt(EmployColumon.HEIGHT));
		employee.setWeight(rs.getInt(EmployColumon.WEIGHT));
		employee.setEnglishName(rs.getString(EmployColumon.ENG_NAME));
		employee.setChineseName(rs.getString(EmployColumon.CH_NAME));
		employee.setPhone(rs.getString(EmployColumon.PHONE));
		employee.setEmail(rs.getString(EmployColumon.EMAIL));
		employee.setBmi(rs.getFloat(EmployColumon.BMI));		
		return employee;
	}
	
	public void convertForInsert(Employee employee, PreparedStatement stmt) throws SQLException {
		stmt.setInt(1, employee.getHeight());
		stmt.setInt(2, employee.getWeight());
		stmt.setString(3, employee.getEnglishName());
		stmt.setString(4, employee.getChineseName());
		stmt.setString(5, employee.getPhone());
		stmt.setString(6, employee.getEmail());
		stmt.setFloat(7, employee.getBmi());
	}
	
	public void convertForUpdate(Employee employee, PreparedStatement stmt) throws SQLException {
		stmt.setInt(1, employee.getId());
		stmt.setInt(2, employee.getHeight());
		stmt.setInt(3, employee.getWeight());
		stmt.setString(4, employee.getEnglishName());
		stmt.setString(5, employee.getChineseName());
		stmt.setString(6, employee.getPhone());
		stmt.setString(7, employee.getEmail());
		stmt.setFloat(8, employee.getBmi());
	}
	
}
