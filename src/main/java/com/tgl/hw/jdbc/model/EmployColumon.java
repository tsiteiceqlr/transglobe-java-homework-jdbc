package com.tgl.hw.jdbc.model;

public final class EmployColumon {
	
	private EmployColumon() {}
	
	public static final String ID = "ID";
	public static final String HEIGHT = "HEIGHT";
	public static final String WEIGHT = "WEIGHT";
	public static final String ENG_NAME = "ENGLISH_NAME";
	public static final String CH_NAME = "CHINESE_NAME";
	public static final String PHONE = "PHONE";
	public static final String EMAIL = "EMAIL";
	public static final String BMI = "BMI";
}
