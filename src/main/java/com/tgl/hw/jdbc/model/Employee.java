package com.tgl.hw.jdbc.model;

public class Employee {
	
	public enum Search {
		ID, HEIGHT, WEIGHT, ENGLISH_NAME, CHINESE_NAME, PHONE, EMAIL
	}
	
	public enum Sort {
		ID, HEIGHT, WEIGHT, PHONE, EMAIL
	}
		
	public enum Max {
		HEIGHT, WEIGHT, BMI
	}
	
	public enum Min {
		HEIGHT, WEIGHT, BMI
	}
	
	private int id;
	private int height;
	private int weight;
	private String englishName;
	private String chineseName;
	private String phone;
	private String email;
	private float bmi;

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getHeight() {
		return height;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}
	
	public int getWeight() {
		return weight;
	}
	
	public void setWeight(int weight) {
		this.weight = weight;
	}
	
	public String getEnglishName() {
		return englishName;
	}
	
	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}
	
	public String getChineseName() {
		return chineseName;
	}
	
	public void setChineseName(String chineseName) {
		this.chineseName = chineseName;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public float getBmi() {
		return bmi;
	}

	public void setBmi(float bmi) {
		this.bmi = bmi;
	}
	
	@Override
	public String toString() {
		return "employee [height=" + height + ", weight=" + weight + ", englishName=" + englishName +", chineseName=" + chineseName + ", phone=" + phone + ", email=" + email + ", id=" + id + ", bmi=" + getBmi() + "]\n";
	}
	
}
