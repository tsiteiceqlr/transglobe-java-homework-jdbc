package com.tgl.hw.jdbc.controller;

import java.sql.Connection;
import java.util.List;

import com.tgl.hw.jdbc.dao.EmployeeDao;
import com.tgl.hw.jdbc.model.Employee;
import com.tgl.hw.jdbc.service.EmployeeService;
import com.tgl.hw.jdbc.util.EmployeeNotValidException;
import com.tgl.hw.jdbc.util.EmployeeValidation;

public class EmployeeController {

	private EmployeeService employeeService;

	public EmployeeController(Connection con, String filePath) {
		employeeService = new EmployeeService(con, filePath);
	}

	public Employee insert(Employee employee) throws EmployeeNotValidException {
		boolean isValid = EmployeeValidation.isValid(employee);
		if (!isValid) {
			throw new EmployeeNotValidException(String.format("employee data is not valid: %s", employee));
		}
		return employeeService.insert(employee);
	}

	public boolean delete(int employeeId) {
		return employeeService.delete(employeeId);
	}

	public boolean update(Employee employee) throws EmployeeNotValidException {
		boolean isValid = EmployeeValidation.isValid(employee);
		if (!isValid) {
			throw new EmployeeNotValidException(String.format("employee data is not valid: %s", employee));
		}
		return employeeService.update(employee);
	}

	public List<Employee> search(Employee.Search factor, String searchVal) {
		return employeeService.search(factor, searchVal);
	}

	public List<Employee> sort(Employee.Sort factor) {
		return employeeService.sort(factor);
	}

	public List<Employee> sort(Employee.Sort factor, EmployeeDao.ORDER order) {
		return employeeService.sort(factor, order);
	}

	public Employee max(Employee.Max factor) {
		return employeeService.max(factor);
	}

	public Employee min(Employee.Min factor) {
		return employeeService.min(factor);
	}

	public int size() {
		return employeeService.size();
	}

	public boolean truncate() {
		return employeeService.truncate();
	}

}
