package com.tgl.hw.jdbc.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.tgl.hw.jdbc.model.Employee;

public final class EmployeeValidation {

	private static final Pattern ENG_NAME_PATTERN = Pattern.compile("^[a-zA-Z-\\s]+");

	private static final Pattern PHONE_PATTERN = Pattern.compile("[0-9]{4}");

	private static final Pattern EMAIL_PATTERN = Pattern.compile("^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$");

	private EmployeeValidation() {
	}

	public static boolean isValid(Employee employee) {
		Matcher matcher = null;
		boolean isValid = true;
		
		// check English name
		matcher = ENG_NAME_PATTERN.matcher(employee.getEnglishName());
		isValid = matcher.matches();
		if (!isValid) {
			return false;
		}
		
		// check Chinese name
		String chName = employee.getChineseName();
		for (int i = 0; i < chName.length(); i++) {
			int codepoint = chName.codePointAt(i);
			if (Character.UnicodeScript.of(codepoint) != Character.UnicodeScript.HAN) {
				return false;
			}
		}
		
		// check phone
		matcher = PHONE_PATTERN.matcher(employee.getPhone());
	    isValid = matcher.matches();
	    if (!isValid) {
	      return false;
	    }
	    
	    // check email
	    matcher = EMAIL_PATTERN.matcher(employee.getEmail());
	    isValid = matcher.matches();
	    if (!isValid) {
	      return false;
	    }

		return true;
	}

}
