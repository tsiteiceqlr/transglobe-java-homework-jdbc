package com.tgl.hw.jdbc.util;

public class DataUtil {

	private DataUtil() {
	}

	public static String msakChinessName(String chName) {
		if (chName == null || chName.length() == 0) {
			return "";
		}

		StringBuilder mask = new StringBuilder();
		mask.append(chName.substring(0, 1));
		for (int i = 0; i < chName.length() - 1; i++) {
			mask.append("*");
		}
		mask.append(chName.substring(chName.length() - 1, chName.length()));

		return mask.toString();
	}

	public static float bmi(int height, int weight) throws EmployeeNotValidException {
		if (weight <= 0 || height <= 0) {
			throw new EmployeeNotValidException(String.format("employee height/weight is not valid: %s, %s", height, weight));
		}

		return ((float) weight / (float) Math.pow(((float) height / 100), 2));
	}

}
